// List of default subscriptions
var defaultSubs = [{
    'subId': 1,
    'url': 'https://www.bbc.co.uk/news',
    'hiddenDay': null
}, {
    'subId': 2,
    'url': 'https://www.nytimes.com/',
    'hiddenDay': null
}, {
    'subId': 3,
    'url': 'https://www.theglobeandmail.com/',
    'hiddenDay': null
}]

// See if there are any saved subscriptions
var savedSubs = localStorage.getItem('paperboySubs')

// If there are, then set them as active, if not then save the defaults
if (savedSubs) {
    var userSubs = JSON.parse(savedSubs)
} else {
    var userSubs = defaultSubs
    localStorage.setItem('paperboySubs', JSON.stringify(userSubs));
}

subCtr = document.getElementById("totalCounter")
subCtr.innerText = 0
unCtr = document.getElementById("unreadCounter")
unCtr.innerText = 0

userSubs.forEach(function (entry) {
    subCtr.innerText = parseInt(subCtr.innerText) + 1;
    if (entry['hiddenDay'] != getDate()) {
        unCtr.innerText = parseInt(unCtr.innerText) + 1;
    }
});