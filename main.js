// List of default subscriptions
var defaultSubs = [{
    'subId': 1,
    'url': 'https://www.bbc.co.uk/news',
    'hiddenDay': null
}, {
    'subId': 2,
    'url': 'https://www.nytimes.com/',
    'hiddenDay': null
}, {
    'subId': 3,
    'url': 'https://www.theglobeandmail.com/',
    'hiddenDay': null
}]

// See if there are any saved subscriptions
var savedSubs = localStorage.getItem('paperboySubs')

// If there are, then set them as active, if not then save the defaults
if (savedSubs) {
    var userSubs = JSON.parse(savedSubs)
} else {
    var userSubs = defaultSubs
    localStorage.setItem('paperboySubs', JSON.stringify(userSubs));
}

var currentSubId

function renderPaperboyList() {
    // Get the last subId used for a subscription
    currentSubId = _.max(userSubs, function (sub) {
        return sub.subId;
    })['subId'];

    // Set the current Subscribed and Unread counts
    subCtr = document.getElementById("totalCounter")
    subCtr.innerText = 0
    unCtr = document.getElementById("unreadCounter")
    unCtr.innerText = 0

    // For each userSub
    userSubs.forEach(function (entry) {
        li = addSub(entry['url'], subId = entry['subId']);
        if (entry['hiddenDay'] == getDate()) {
            li.style.textDecoration = "line-through";
            li.style.pointerEvents = 'none';
            li.firstElementChild.style.pointerEvents = 'auto';
        } else {
            unCtr.innerText = parseInt(unCtr.innerText) + 1;
        }
    });
}

renderPaperboyList()

// Get the input field
var input = document.getElementById("urlBox");

// Press addBtn on enter
input.addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
        document.getElementById("addBtn").click();
    }
});

// // Create a "close" button and append it to each list item
// var myNodelist = document.getElementsByTagName("LI");
// var i;
// for (i = 0; i < myNodelist.length; i++) {
//     var span = document.createElement("SPAN");
//     var txt = document.createTextNode("\u00D7");
//     span.className = "close";
//     span.appendChild(txt);
//     myNodelist[i].appendChild(span);
// }

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
    close[i].onclick = function () {
        var li = this.parentElement;
        var targetIndex = _.findIndex(userSubs, function (sub) {
            return sub['subId'] == li.getAttribute('subId');
        });
        var hiddenDay = _.find(userSubs, function (sub) {
            return sub['subId'] == li.getAttribute('subId')
        })['hiddenDay'];

        userSubs.splice(targetIndex, 1);
        localStorage.setItem('paperboySubs', JSON.stringify(userSubs));
        li.parentNode.removeChild(li);

        subCtr.innerText = parseInt(subCtr.innerText) - 1
        if (hiddenDay != getDate()) {
            unCtr.innerText = parseInt(unCtr.innerText) - 1
        }
    }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function (event) {
    if (event.target.tagName === 'LI') {
        unCtr.innerText = parseInt(unCtr.innerText) - 1
        var hiddenDay = getDate()
        window.open(event.target.getAttribute('targetLink'));
        event.target.style.textDecoration = "line-through";
        event.target.style.pointerEvents = 'none';
        event.target.firstElementChild.style.pointerEvents = 'auto';
        var targetIndex = _.findIndex(userSubs, function (sub) {
            return sub['subId'] == event.target.getAttribute('subId');
        });
        userSubs[targetIndex]['hiddenDay'] = hiddenDay
        localStorage.setItem('paperboySubs', JSON.stringify(userSubs))
    }
}, false);

setInterval(function () {
    var myNodelist = document.getElementsByTagName("LI");
    var i;
    for (i = 0; i < myNodelist.length; i++) {
        var targetIndex = _.findIndex(userSubs, function (sub) {
            return sub['subId'] == myNodelist[i].getAttribute('subId');
        });
        var hiddenDay = _.find(userSubs, function (sub) {
            return sub['subId'] == myNodelist[i].getAttribute('subId')
        })['hiddenDay'];
        if (hiddenDay != null & hiddenDay != getDate()) {
            myNodelist[i].style.textDecoration = "none";
            myNodelist[i].style.pointerEvents = 'auto';
            userSubs[targetIndex]['hiddenDay'] = null;
            unCtr.innerText = parseInt(unCtr.innerText) + 1
        }
    }
}, 5000); // 60 * 1000 milsec

function addSubOnClick(subId = null) {
    var inputValue = document.getElementById("urlBox").value;
    addSub(inputValue, subId = subId)
}

function addSub(url, subId = null) {
    if (url === '') {
        return;
    } else if (!validURL(url)) {
        alert('You must enter a URL of the form "http://www.example.com"')
        return;
    } else {
        var li = document.createElement("li");
        var t = document.createTextNode(url);
        li.appendChild(t);
        document.getElementById("paperboyList").appendChild(li);
    }

    li.setAttribute('targetLink', url);

    subCtr.innerText = parseInt(subCtr.innerText) + 1;

    if (subId) {
        li.setAttribute('subId', subId);
    } else {
        currentSubId = currentSubId + 1
        userSubs.push({
            'subId': currentSubId,
            'url': url
        })
        localStorage.setItem('paperboySubs', JSON.stringify(userSubs));
        li.setAttribute('subId', currentSubId)
        unCtr.innerText = parseInt(unCtr.innerText) + 1;
    }

    document.getElementById("urlBox").value = "";

    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    li.appendChild(span);

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.display = "none";
            var li = this.parentElement;
            var targetIndex = _.findIndex(userSubs, function (sub) {
                return sub['subId'] == li.getAttribute('subId');
            });
            var hiddenDay = _.find(userSubs, function (sub) {
                return sub['subId'] == li.getAttribute('subId')
            })['hiddenDay'];
            userSubs.splice(targetIndex, 1);
            localStorage.setItem('paperboySubs', JSON.stringify(userSubs));

            li.parentNode.removeChild(li);
            subCtr.innerText = parseInt(subCtr.innerText) - 1
            if (hiddenDay != getDate()) {
                unCtr.innerText = parseInt(unCtr.innerText) - 1
            }
        }
    }

    return li
}

function resetSubs() {
    if (confirm("Do you want to reset to the default list?") == true) {
        userSubs = defaultSubs
        localStorage.setItem('paperboySubs', JSON.stringify(userSubs));
        var paperboyList = document.getElementById("paperboyList")
        while (paperboyList.firstChild) {
            paperboyList.removeChild(paperboyList.firstChild);
        }
        renderPaperboyList()
    }
}

$('.close').on('mouseover', function () {
    $(this).parent().addClass('hover-close');
}).on('mouseout', function () {
    $(this).parent().removeClass('hover-close');
})