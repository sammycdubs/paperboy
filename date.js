var dateField = document.getElementById('date')

var today = new Date();
var dayN = today.getDay();
var monthN = today.getMonth();

var weekdayNames = ["SUNDAY", "MONDAY", "TUESDAY",
    "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"
];

var monthNames = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE",
    "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"
];

function ordinalSuffix(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

dateField.innerText = weekdayNames[dayN] + ', ' +
    monthNames[monthN] + ' ' + ordinalSuffix(today.getDate()) + ', ' +
    today.getFullYear()